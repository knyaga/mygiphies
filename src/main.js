$(document).ready(function () {

    //Initialize gallery
    Gallery.init();

    //Infinite scroll
    $(window).scroll(function () {
      if ($(window).scrollTop() + $(window).height() == $(document).height()) {
        Gallery.init();
      }
    });

    //Gallery popup
    $("#my-gallery").magnificPopup({
      delegate: 'a',
      type: 'image',
      gallery: {
        enabled: true
      }
    });

    //Refresh action
    $("#refresh-icon").on('click', function(){
        Gallery.reset();
        Gallery.init();
    });


  });