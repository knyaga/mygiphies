var Gallery = (function ($, document) {

    var offset = 0;
    var limit = 25;
    var host = "https://api.giphy.com/v1/gifs/trending?";
    var api_key = "api_key=e31e45220be04dd583208d4337a8144d";

    var $galleyContainer = document.querySelector('#my-gallery');

    function init() {
        _fetchGiphiesData();
    }

    function _fetchGiphiesData() {
        _loadGiphies(offset).done(function (data) {
            var giphies = data.data;
            offset += limit;
            return giphies.map(function (giphy) {
                _renderGallery(giphy);
            });
        });
    }

    function _loadGiphies(offset) {
        var url = host + api_key + "&limit=" + limit + "&offset=" + offset;
        var ajax = $.ajax({
            url: url,
            method: "GET",
            headers: { "Accept": "application/json" },
            error: function (err) {
                console.log(err);
            }
        });
        return ajax;
    }

    function _renderGallery(data) {
        var imgUrl = data.images.original.url;
        var div = _createThumbnail();
        var a = div.querySelector('a');
        var img = new Image();

        div.id = data.id;
        a.href = imgUrl;
        img.className = "img-fluid";
        img.src = imgUrl;

        img.onload = function () {
            _appendNodeElement(a, img);
            _appendNodeElement(div, a);
            _appendNodeElement($galleyContainer, div);
        }
    }

    function _createThumbnail() {
        var div = _createNodeElement('DIV');
        var a = _createNodeElement('A');
        div.className = 'col-lg-3 col-md-4 col-xs-6';
        _appendNodeElement(div, a);

        return div;
    }

    function _createNodeElement(el) {
        return document.createElement(el);
    }

    function _appendNodeElement(parent, el) {
        return parent.appendChild(el);
    }

    function resetGallery() {
        $("#my-gallery").empty();
        offset = 0;
    }

    return {
        init: init,
        reset: resetGallery
    }
})($, document);
